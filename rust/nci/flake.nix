{
  inputs = {
    nci.url = "github:yusdacra/nix-cargo-integration";
  };

  outputs = { nci, ... } @ inputs:
    let
      outputs = nci.lib.makeOutputs {
        root = ./.;
        config = import ./nix/nci-config.nix inputs;
      };
    in
    outputs;
}
