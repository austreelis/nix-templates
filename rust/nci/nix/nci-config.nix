{ nci, ... }:
{ pkgs, ... }:
let
  inherit (pkgs) lib;
  inherit (lib) lists recursiveUpdate;

  commandsFilter = { package ? null, ... }:
    # NCI includes alejandra for nix code formatting by default but we use
    # nixpkgs-fmt
    if package.pname or null == "alejandra" then false
    else true;
in
{
  builder = "crane";

  cCompiler = {
    enable = true;
    package = pkgs.clang;
  };

  outputs.defaults = {
    package = "nix-templates-rust";
    app = "nix-templates-rust";
  };

  shell = prev:
    recursiveUpdate prev {
      devshell = {
        name = "nix-templates-rust";
        packages = [
          pkgs.cargo-audit
          pkgs.cargo-bloat
          pkgs.cargo-expand
          pkgs.cargo-deny
          pkgs.cargo-llvm-cov
          pkgs.cargo-mommy # You didn't know you needed it
          pkgs.libllvm
        ];
      };
      commands =
        lists.filter commandsFilter prev.commands
        ++ [
          {
            package = pkgs.nixpkgs-fmt;
            category = "formatting";
          }
          {
            package = pkgs.lldb;
            category = "rust";
          }
          {
            package = lib.hiPrio pkgs.rust-analyzer;
            category = "rust";
          }
        ];
    };
}
