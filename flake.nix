{
  description = ''
    A collection of flake templates
  '';

  outputs = _: {
    templates = {
      rust = {
        path = ./rust/nci;
        description = ''
          A basic rust crate, using nix-cargo-integration.
        '';
      };
    };
  };
}
